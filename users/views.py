from django.views.generic import CreateView
from django.urls import reverse_lazy
from .forms import CreateUserForm

class SignUpView(CreateView):
    form_class= CreateUserForm
    success_url= reverse_lazy('login')
    template_name= 'signup.html'

