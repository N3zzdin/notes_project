from django.contrib import admin
from .models import CustomUser
from .forms import CreateUserForm
from django.contrib.auth.admin import UserAdmin




class CustomUserAdmin(UserAdmin):
    add_form= CreateUserForm
    model= CustomUser

admin.site.register(CustomUser, CustomUserAdmin)